# Borgbuddy

Paired remote backups made easy: keep a backup of your data in some friends' disk while your friend stores their data in yours. And do it privately (only you can see your data). 

_"Borg Buddies always have your back."_

## The idea

You want to have a private NAS for your data. And you want to backup the data. Not only locally but also remotely to prevent data loss in case of a fire, burglary, etc.

You could use some service for the remote backup, but you prefer a DIY and cheaper solution.

Just agree with someone else to become backup buddies: you provide your buddy with storage space for his/her backup in your NAS and your buddy does the same for you.

BorgBuddy is just a tool to simplify pairing of NAS devices to do exactly that in a private and secure way:

- it uses borg and borgmatic to create local and remote backup (the data is encrypted locally before being sent... so not even your buddy can access your data)
- it transfers data between the buddies via a hidden Tor ssh service (to avoid having to open ports in your router firewall)
- The "pairing" of the NAS devices is done by a one-time exchange of public keys and of the hidden service address.
- everything is implemented as a set of containers that can be run with `docker-compose`


## Example:

Alice and Bob decided to become borg buddies. They do not yet have a NAS at home so they each buy a RaspberryPi and 2 external USB3 disks.
They install raspbian on them,  and run:

- In Alice's NAS:
```console
$ docker-compose up -d
$ borgbuddy init
$ borgbuddy export alice.bb
```

- In Bob's NAS:
```console
$ docker-compose up -d
$ borgbuddy init
$ borgbuddy export bob.bb
```

Now they exchange the `.bb` files over a secure channel (e.g. they hand them over a USB pendrive) and just import each other's info:

- In Alice's NAS:
```console
$ borgbuddy import bob.bb
```

- In Bob's NAS:
```console
$ borgbuddy import alice.bb
```

Done!
## Status

This is work-in-progress (spect slow dev since I devote time to this very occasionally) 

TO-DO list:

- First version of borgbuddy-client service 
  - [x] ssh running
  - [x] tor running
  - [ ] borgmatic tested for local backup
  - [ ] borgmatic tested for remote backup
  - [ ] cron tested
  - [ ] handle mapping of host source dir to client /source 
- First version of borgbuddy-server service
  - [x] sshd running and passwd disabled
  - [x] tor running
  - [x] ssh-onion service running
  - [x] authorized-keys configured (with empty file)
- First version of admin service
  - [x] provide skeleton of borgmatic CLI admin service 
- First version of borgbuddy py module
  - [x] borgbuddy py module skeleton
  - minimal init subcommand:
    - [x] create ssh key pairs (for append and rw access)
    - [x] ask for own backup passphrase and configure it in borgmatic.yml
    - [x] configure a daily backup with cron
    - [x] create own repo
  - minimal export subcommand:
    - [ ] implement get_info() (gathers all info as a dict)
    - [ ] export public keys
    - [ ] export onion address
    - [ ] encrypt and hash
  - minimal import subcommand:
    - [ ] add hash check
    - [ ] import key pairs
    - [ ] import onion address
    - [ ] create buddy repo on server
- [ ] Allow to configure cron times configuration on borgbuddy init 
- [ ] Add test compose file with Alice and Bob nodes communicating with each other
- Harden server: use tor hidden service client authorization (see man torrc)
  - [ ] generate tor service keys in client
  - [ ] handle the pub key with borgbuddy export and import
- [ ] Provide a systemd service file to start borgbuddy on boot
- [ ] Provide minimal webadmin service (e.g. flask?)
- [ ] Auto build and push images to registry
  - [ ] amd64
  - [ ] arm64 
- [ ] test on RPi
- [ ] reduce overall images size (use a base image, etc)
- Improve monitoring
  - [ ] implement alert emails on missing buddy backups
  - [ ] work on providing easy access to logs, etc
  - [ ] integrate with service monitoring tools (prometheus, grafana,...)
- [ ] Consider alternative pairing methods (bluetooth, wifi broadcast,...)
- [ ] Document/support avoiding Tor bottleneck by using alternative connection methods:
  - [ ] opening a port in the NAT for allowing direct ssh borg connections
  - [ ] try to find a nat hole punching method usable wit an oniion service relay
