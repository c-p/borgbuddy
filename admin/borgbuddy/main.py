"""Main module for borgbuddy"""

import logging
import shutil
import subprocess

from pathlib import Path
from string import Template

import click
import yaml

from .utils import ask_passwd, write_rsa_key_pair


logging.basicConfig()
logger = logging.getLogger("borgbuddy")
logger.setLevel(logging.INFO)

BUDDY_QUOTA = "500G"

RESOURCES_PATH = Path(__file__).parent / "resources"

HELP_BORGMATIC_PASSWORD = """
Both your local and remote backups will be encrypted with a passphrase.
The passphrase will be stored locally so that borg can use it. The 
encryption will be performed locally so only someone with full access 
to this host (in principle that is only you) can see it. 

As an example: Alice can't see Bob's files even when she stores Bob's 
remote backup, because Bob's passphrase never leaves Bob's host.

Note 1: someone with full access to this host already has access to the 
source data anyway, in which case they do not need the key at all.

Note 2: It is very important that you write down or store this 
passphrase somewhere safe. Without it, your backups will not be 
recoverable.
"""

HELP_FULL_ACCESS_KEY_PASSWORD = """
By default, the automatic remote backup is done in "append-only" mode.
Full write access is only granted with a special key that requires 
a password to be used. This protects against ransomware encrypting or 
deleting your backup, while still allows you to manually delete remote 
files from your remote backup by manually entering the full access 
password (e.g. to free space).

However, if you prefer (at your own risk) to also automate deletions, then
set an empty password here.

"""


@click.group()
def borgbuddy():
    """BorgBuddy main command"""
    return


@click.command()
@click.option(
    "--client-conf",
    type=click.Path(exists=True, file_okay=False, dir_okay=True),
    help="Path to client config dir.",
    default="/etc/borgbuddy/client",
)
@click.option(
    "--server-conf",
    type=click.Path(exists=True, file_okay=False, dir_okay=True),
    help="Path to server config dir.",
    default="/etc/borgbuddy/server",
)
@click.option("--debug-pwd", default=b"", help="Password (use only for debugging)")
def init_cmd(client_conf, server_conf, debug_pwd):
    """Prepares this device to be a borg buddy"""

    client_conf = Path(client_conf)
    server_conf = Path(server_conf)

    use_append_mode = True
    debug_pwd = bytes(debug_pwd, "ascii")

    logger.info("Initializing buddy")

    # Create /etc/borgbuddy/client/borgmatic.cron
    borgmatic_cron = client_conf / "borgmatic.cron"
    if not borgmatic_cron.is_file():
        logger.info("Creating %s", borgmatic_cron)
        shutil.copy(RESOURCES_PATH / "borgmatic.cron", borgmatic_cron)
    else:
        logger.warning("The %s file already exists.", borgmatic_cron)

    # create 2 private/public key pairs in ~etc/borgbuddy/client/ :
    #       - borgbuddy_append_rsa{,.pub} (no password, for auto backup,
    #         but it will only be authorised to append)
    #       - borgbuddy_full_rsa{,.pub} (with passwd to protect from ransomware)
    borgbuddy_append_rsa = client_conf / "borgbuddy_append_rsa"
    if not borgbuddy_append_rsa.is_file():
        logger.info("Creating %s", borgbuddy_append_rsa)
        write_rsa_key_pair(borgbuddy_append_rsa)
    else:
        logger.warning(
            "A '%s' file already exists. Skipping creation of append-only key",
            borgbuddy_append_rsa,
        )

    borgbuddy_full_rsa = client_conf / "borgbuddy_full_rsa"
    if not borgbuddy_full_rsa.is_file():
        print(HELP_FULL_ACCESS_KEY_PASSWORD)
        password = debug_pwd or ask_passwd(
            prompt="Enter password for full access to the remote backup: ", min_length=0
        )

        if not password:
            password = None
            use_append_mode = False

        logger.info("Creating %s", borgbuddy_full_rsa)
        write_rsa_key_pair(borgbuddy_full_rsa, password=password)

    else:
        logger.warning(
            "A '%s' file already exists. Skipping creation of full access key",
            borgbuddy_full_rsa,
        )

    # Create (empty) /etc/borgbuddy/server/authorized_keys file
    authorized_keys = server_conf / "authorized_keys"
    if not authorized_keys.is_file():
        logger.info("Creating %s", authorized_keys)
        shutil.copy(RESOURCES_PATH / "authorized_keys", authorized_keys)
        authorized_keys.chmod(0o644)
    else:
        logger.warning("The '%s' file already exists.", authorized_keys)

    # Create /etc/borgbuddy/client/borgmatic.yml with local repository
    # (remote to be added on import)
    borgmatic_yml = client_conf / "borgmatic.yml"
    if not borgmatic_yml.is_file():
        logger.info("Creating %s", borgmatic_yml)
        yml_template = Template((RESOURCES_PATH / "borgmatic_config.tpt").read_text())

        print(HELP_BORGMATIC_PASSWORD)
        passphrase = debug_pwd or ask_passwd(
            prompt="Enter passphrase for encrypting the backup: "
        )

        if use_append_mode:
            identity = borgbuddy_append_rsa.resolve()
        else:
            identity = borgbuddy_full_rsa.resolve()

        yml = yml_template.safe_substitute(
            passphrase=passphrase.decode("ascii"),
            identity=identity,
        )
        borgmatic_yml.write_text(yml)

    else:
        logger.warning("The '%s' file already exists.", borgmatic_yml)

    # initialize own local borg repo
    logger.info("Initializing local repository")
    ret = subprocess.run(
        ["borgmatic", "init", "--encryption", "repokey", "-c", borgmatic_yml],
        check=False,
        capture_output=True,
    )
    ret.check_returncode()


@click.command()
@click.argument("filename", type=click.Path())
def export_cmd(filename):
    """Exports this device info to be passed to its buddy."""
    print("exporting buddy info")
    # TODO: create json containing:
    #       - onion_address+reponame
    #       - pubkeys (from ~/.ssh/borgbuddy_{append,full}_rsa.pub)


@click.command()
@click.argument("filename", type=click.Path())
def import_cmd(filename):
    """Imports info from a buddy to pair with it."""
    print("importing buddy info")
    # TODO: Read info file to get buddy's onion_address+reponame and pubkeys
    # TODO: (!) sanitize pubkeys (see https://borgbackup.readthedocs.io/en/stable/deployment/hosting-repositories.html)
    # TODO: Add buddy pubkeys to $(BUDDY_HOME)/.ssh/authorized_keys
    # TODO: Add onion_address+reponame to "repositories" in /etc/borgbuddy/borgmatic.yml
    # TODO: store info file in /etc/borgbuddy/<buddy>.bb to allow removing


@click.command()
def remove_cmd():
    """Revert changes done by import"""

    print("Removing buddy config")
    # TODO: retrieve buddy info from /etc/borgbuddy/<buddy>.bb
    # TODO: interactively ask confirmation for removing buddy pubkeys
    # TODO: interactively ask confirmation for removing buddy remote repo
    # TODO: interactively ask confirmation for removing local buddy's backup (!)
    # TODO: remove /etc/borgbuddy/<buddy>.bb


@click.command()
def clean_cmd():
    """Revert changes done by init"""

    print("Disabling ")
    # TODO: call remove for all buddies found in /etc/borgbuddy/*.bb
    # TODO: interactively ask confirmation for removing borgbuddy user
    # TODO: interactively ask confirmation for stopping & removing tor hidden ssh service
    # TODO: interactively ask confirmation for removing own local backup (!)
    # TODO: interactively remove /etc/borgbuddy/* and /etc/systemd/system/borgbuddy.*
    # TODO: interactively ask confirmation to remove private/pub keys in ~/.ssh/borgbuddy_*


borgbuddy.add_command(init_cmd, name="init")
borgbuddy.add_command(export_cmd, name="export")
borgbuddy.add_command(import_cmd, name="import")
borgbuddy.add_command(remove_cmd, name="remove")
borgbuddy.add_command(clean_cmd, name="clean")


if __name__ == "__main__":
    borgbuddy()
