"""
Miscellaneous utility methods for borgbuddy
"""

from pathlib import Path
from getpass import getpass

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend


def write_rsa_key_pair(path, password=None, key_size=2048):
    """Generates and writes a priv/pub key pair of files"""

    key = rsa.generate_private_key(
        backend=default_backend(), public_exponent=65537, key_size=key_size
    )

    if password is None:
        encryption_algorithm = serialization.NoEncryption()
    else:
        encryption_algorithm = serialization.BestAvailableEncryption(password)

    private_key = key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=encryption_algorithm,
    )

    public_key = key.public_key().public_bytes(
        encoding=serialization.Encoding.OpenSSH,
        format=serialization.PublicFormat.OpenSSH,
    )

    prv_path = Path(path)
    prv_path.write_bytes(private_key)
    prv_path.chmod(0o600)

    pub_path = prv_path.with_suffix(".pub")
    pub_path.write_bytes(public_key)
    pub_path.chmod(0o644)


def ask_passwd(prompt="Password: ", min_length=8):
    """prompt the user for a password enforcing rules and asking for verification"""
    invalid = f"""
    Invalid. Password must be at least {min_length} printable ASCII characters
    mixing upper and lower case letters, numbers and symbols.
    """
    while True:
        pwd = getpass(prompt="\n" + prompt)
        if len(pwd) < min_length or not pwd.isascii() or not pwd.isprintable():
            print(invalid)
            continue
        if pwd and (pwd.islower() or pwd.isupper() or pwd.isalnum()):
            print(invalid)
            continue

        confirm = getpass(prompt="\nReenter to confirm: ")
        if pwd != confirm:
            print("Passwords differ. Try again")
            continue
        return bytes(pwd, "ascii")
