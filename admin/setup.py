from setuptools import setup

setup(
    name="borgbuddy",
    version="0.0.1",
    packages=["borgbuddy"],
    package_data={"borgbuddy": ["resources/*"]},
    install_requires=[
        "click",
        "cryptography",
        "pyyaml",
    ],
    entry_points = {
        "console_scripts": [
            "borgbuddy = borgbuddy.main:borgbuddy",
        ]
    }
)